//
//  TestViewController.swift
//  LukaiOSSDK
//
//  Created by José Daniel Gómez on 21/9/21.
//

import Foundation
import UIKit

class TestViewController : UIViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let credentials = LukaAuthCredentials(userName: "ridery", password: "short-rope-northern-quickly")

        let luka: LukaApi = Luka.initialize(credentials: credentials, forDebug: true)
        
        // needed to launch the bluesnap form
        luka.setup(controller: self.navigationController!)
        
        let email = "sample@sample.com"
        let customerId = "11111111-1111-1111-1111-111111111111"
        
        addCustomerCard(api: luka, email: email, customerId: customerId)
        cards(api: luka, email: email, customerId: customerId)
        charge(api: luka)
        checkTransaction(api: luka)
        deleteCustomerCard(api: luka, cardId: 0, customerId: customerId)
        

    }
    
    func charge(api: LukaApi) {
            
        let params = LukaPaymentParams(
            method: CreditCardMethod(),
            amount: 97.52,
            currency: .usd,
            email: "joseg@manzanares.com.ve"
        )
        
        api
            .createPaymentRequest(params: params)
            .registryOnProgress { progress in
                
                switch progress.acton {
                case .setup:
                    print("Payment process setup started.")
                    // Payment progcess was launched
                    // The progress.traceId is null in this case
                case .begin:
                    // payment process setup was successful. Starting the payment process.
                    // now the traceId is available.
                    // store traceId somewhere safe. If something happens, you can recover the
                    // transaction status with the traceId
                    print("traceId \(progress.traceId)")
                default:
                    break
                }
                
                print("LukaApi: progress -> \(progress)")
            }
            .registryOnSuccess { response in
                print("LukaApi: payment success \(response.currency.symbol)\(response.amount)")
            }
            .registryOnError { lukaError in
                print("LukaApi: error -> \(lukaError.message)")
            }
            .execute()
            
    }
    
    func checkTransaction(api: LukaApi) {
        
        api
            .checkTransaction(traceId: "2802afa2-e684-48af-8a59-edbb694b8d1b")
            .registryOnSuccess { result in
                print("LukaApi: check success -> \(result)")
            }
            .registryOnError { lukaError in
                print("LukaApi: error -> \(lukaError.message)")
            }
            .execute()
        
    }
    
    func addCustomerCard(api: LukaApi, email: String, customerId: String?) {
        
        api
            .addCustomerCardRequest(email: email, lukaCustomerId: customerId)
            .registryOnProgress { progress in
                print("LukaApi: progress -> \(progress)")
            }
            .registryOnSuccess { response in
                print("LukaApi: store success -> \(response)")
            }
            .registryOnError { lukaError in
                print("LukaApi: error -> \(lukaError.message)")
            }
            .execute()
        
    }
    
    func cards(api: LukaApi, email: String, customerId: String) {
        
        api
            .indexCustomerCardsRequest(lukaCustomerId: customerId)
            .registryOnSuccess { cards in
                print("LukaApi: cards -> \(cards)")
            }
            .registryOnError { lukaError in
                print("LukaApi: error -> \(lukaError.message)")
            }
            .execute()
        
    }
    
    func chargeWithCard(creditCard: CreditCard, api: LukaApi, email: String, customerId: String) {
        
        let params = LukaCardPaymentParams(customerId: customerId, card: creditCard, amount: 97.52, currency: LukaCurrency.usd, email: email)
        
        api
            .createPaymentRequest(params: params)
            .registryOnProgress { progress in
                
                switch progress.acton {
                case .setup:
                    print("Payment process setup started.")
                    // Payment progcess was launched
                    // The progress.traceId is null in this case
                case .begin:
                    // payment process setup was successful. Starting the payment process.
                    // now the traceId is available.
                    // store traceId somewhere safe. If something happens, you can recover the
                    // transaction status with the traceId
                    print("traceId \(progress.traceId)")
                default:
                    break
                }
                
                print("LukaApi: progress -> \(progress)")
            }
            .registryOnSuccess { response in
                print("LukaApi: payment success \(response.currency.symbol)\(response.amount)")
            }
            .registryOnError { lukaError in
                print("LukaApi: error -> \(lukaError.message)")
            }
            .execute()
        
    }
    
    func deleteCustomerCard(api: LukaApi, cardId: UInt64, customerId: String) {
    
        api
            .deleteCustomerCard(cardId: cardId, lukaCustomerId: customerId)
            .registryOnSuccess { _ in
                print("LukaApi: card rmoved successfully")
            }
            .registryOnError { lukaError in
                print("LukaApi: error -> \(lukaError.message)")
            }
            .execute()
        
    }
    
}
